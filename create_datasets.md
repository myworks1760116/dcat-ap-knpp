
## create nodes & labels

TTL file

```
@prefix dcat: <http://www.w3.org/ns/dcat#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix odrl: <http://www.w3.org/ns/odrl/2/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix spdx: <http://spdx.org/rdf/terms#> .
@prefix time: <http://www.w3.org/2006/time#> .
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix knpp: <http://www.kaeri.re.kr/DCAT-AP-KNPP#> .

knpp:POSRV_STUCK_OPEN_t4f0.07
    a dcat;Catalog ;
    a dcat:Dataset ;
    a dcat:Resource ;

    foaf:homepage <http://dcat.kaeri.re.kr/catalog> ;

    dcat:distribution knpp:POSRV_STUCK_OPEN_t4f0.07-csv ;

    dcat:keyword "POSRV", "stuck", "open" ;
    dcat:contactPoint: <http://dcat.kaeri.re.kr/contact> ;
    dcterms:title "01_POSRV_STUCK_OPEN_t4f0.07" ;
    dcterms:creator ; knpp:Dr_Koo
    dctaerms:issued "2024-06-01"^^xsd:date ;
    dctermss:modified "2024-06-18"^^xsd:date ;
    .

knpp:PZR_SprayVLV_Open_t4f0.5
    a dcat:Dataset ;
    a dcat:Resource ;

    dcat:distribution knpp:PZR_SprayVLV_Open_t4f0.5-csv ;

    dcat:keyword "PZR", "spray", "valve", "open" ;
    dcat:contactPoint: <http://dcat.kaeri.re.kr/contact> ;
    dcterms:title "02_PZR_SprayVLV_Open_t4f0.5" ;
    dcterms:creator knpp:Dr_Chae;
    dctaerms:issued "2024-06-01"^^xsd:date ;
    dctermss:modified "2024-06-18"^^xsd:date ;
    .

knpp:CHRGPP_Abnormal_At1Bt1
    a dcat:Resource ;

    dcat:keyword "PZR", "spray", "valve", "open" ;
    dcat:contactPoint: <http://dcat.kaeri.re.kr/contact> ;
    dcterms:title "03_CHRGPP_Abnormal_At1Bt1" ;
    dcterms:creator knpp:Dr_Chae;
    dctaerms:issued "2024-06-01"^^xsd:date ;
    dctermss:modified "2024-06-17"^^xsd:date ;
    .

knpp:Dr_Koo
    a foaf:Person ;
    foaf:name "Koo, SeoRyong" ;
    foaf:currentProject knpp:project-001 ; 
    .

knpp:Dr_Chae
    a foaf:Person ;
    foaf:name "Chae, Youngho" ;
    foaf:currentProject knpp:project-002 ; 
    .

```

