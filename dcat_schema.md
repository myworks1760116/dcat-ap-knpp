# Schema Design for Neo4J 

## Vocaburary in Neo4J
먼저 Neo4J의 node, relationship, node label 그리고 property에 대한 명확한 이해가 필요:

### Node
Nodes are used to represent entities (discrete objects) of a domain.

#### Node Label
Labels shape the domain by grouping (classifying) nodes into sets where all nodes with a certain label belong to the same set. A node can have zero to many labels.

### Relationship 
A relationship describes how a connection between a source node and a target node are related. It is possible for a node to have a relationship to itself. Relationships must have a type (one type) to define (classify) what type of relationship they are.

    - Connects a source node and a target node.- Has a direction (one direction).
    - Must have a type (one type) to define (classify) what type of relationship it is.
    - Can have properties (key-value pairs), which further describe the relationship.

#### Relation type** 
outgoing/ncoming relationship with direction

### Property
Properties are key-value pairs that are used for storing data on nodes and relationships.

### Schema[^21]
A schema in Neo4j refers to indexes and constraints.

#### Index
Indexes are used to increase performance. 

#### Constraint
Constraints are used to make sure that the data adheres to the rules of the domain.  

### Naming Conventions
Node labels, relationship types, and properties (the key part) are case sensitive, meaning, for example, that the property name is different from the property Name.

The following naming conventions are recommended:

| Graph entity | Recommended style | Example |
|--------------|-------------------|---------|
| Node label | Camel case, beginning with an upper-case character | :VehicleOwner rather than :vehicle_owner |
| Relationship type | Upper case, using underscore to separate words | :OWNS_VEHICLE rather than :ownsVehicle |
| Property | Lower camel case, beginning with a lower-case character | firstName rather than first_name | 

## Vocaburary in DCAT-V3
최근 업데이트된 [DCAT-V3](https://www.w3.org/TR/vocab-dcat-3/)에 따르면, 사용되는 vocaburary는 다음과 같다 (참조 [Figure 1](https://www.w3.org/TR/vocab-dcat-3/#fig-dcat-all-attributes)).

### Class
UML를 사용하는 Class에 해당된다. 작성된 Figure 1에 따르면, Resource Class가 최상위 class이며, 그의 Subclass로 DataSet와 DataService class가 있다. Dataset class의 subclass로 DatasetSeries와 Catalog class가 있는 class hierarchy를 구성하고 있다. 즉 IS-A hierarchy relationship을 구성하고 있다. 또한 이외의 class(예: Distribution class, CatalogRecord class 등)는 class hieararchy 상에 위치 않고 다른 class와 relationship을 구성하여 다른 class의 record(instance)를 연결하여 두 class 간의 relationship을 표현한다(예: Catalog class의 property dcat:record).

### Property
class의 record(instance)를 기술한다. property는 relationship을 깃(예: Catalog class의 property dcat:record)하거나 메타데이터를 기술하는 것(예: Catalog class의 property foaf:homepage)으로 나눌 수 있다.

## DCAT vocaburary의 Neo4j vocaburary로 매핑
DCAT은 기본적으로 메타데이터를 기술하기 위하여 RDF을 채택하고 있다. 그러나 이를 저장하기 위하여 UML을 사용하는 과정에서 graph에는 존재하지 않는 class를 도입하였다. 이는 메터데이터를 공유한다는 장점은 있으나 mismatching을 유발하고 있다. 따라서 class에서 정적인 면과 유사한 node들의 모음(collection)이라는 개념으로 접근하는 것이 바람직할 것이다.

### DCAT-V3의 class
앞서 언급한 바와같이 class는 Neo4J의 label을 이용하여 구현할 수 있을 것이다. 현재 DCAT-V3에는 분류체계 도입을 위한 SKOS 관련 class(`Concept Scheme`과 `Concept`)를 포함하여 15 클래스가 정의되어 있다. 여기에 DCAT-AP-KNPP로 확장을 위한 class[^31]를 추가하여야 한다.  

### DCAT-V3의 property
DCAT-V3의 property는 Neo4J의 property로 직접 매핑할 수 있다.

### DCAT-V3의 relationship
DCAT-V3의 relationship은 Neo4J의 relationship으로 직접 매핑할 수 있다. (단 IS-A relationship 제외)

### 매핑의 예

| DCAT-V3 voc | Neo4J voc |
|-------------|-----------|
| class | node label |
| class instance | node |
| property | property |
| relationship | relationship |

따라서 [DCAT-V3](https://www.w3.org/TR/vocab-dcat-3/#external-vocab)의 `EXAMPLE 1`을 Neo4에 mapping하면 다음과 같이 저장될 수 있다.

![](./fig4_01.png)

![](./fig4_02.png)



[^21]: Neo4J의 Schema는 일반적인 DB에서의 schema와 다르게 정의되어 있다.

[^31]: 현재 `Software`와 `Documents` class를 계획하고 있음.

